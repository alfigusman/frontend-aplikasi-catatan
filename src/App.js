import React, {Component} from "react";
import {Switch, Route, Link } from "react-router-dom";
import {Nav, Navbar, NavDropdown} from "react-bootstrap";
import ls from 'local-storage'


import Home from "./componets/Home"
/* Topics */
import CreateTopics from "./componets/Topics/Create"
import ReadTopics from "./componets/Topics/Read"
import UpdateTopics from "./componets/Topics/Update"
import DeleteTopics from "./componets/Topics/Delete"
/* Posts */
import CreatePosts from "./componets/Posts/Create"
import ReadPosts from "./componets/Posts/Read"
import UpdatePosts from "./componets/Posts/Update"
import DeletePosts from "./componets/Posts/Delete"
import WherePosts from "./componets/Posts/Where"
/* Auth */
import Login from "./componets/Auth/Login"
import Register from "./componets/Auth/Register"
import Logout from "./componets/Auth/Logout"
import NoMatch from "./componets/NoMatch"

import './style/App.css'


class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false
        }
    }

    render() {
        return (
            <div>
                <Navbar bg="light" expand="lg" style={{ marginBottom: '1rem' }}>
                    <Navbar.Brand as={Link} to='/'>Catatan</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to='/'>Home</Nav.Link>
                            {(ls.get('isLogin') ||this.state.isLogin) && (
                                <>
                                    <NavDropdown title="Topics" id="basic-nav-dropdown">
                                        <NavDropdown.Item as={Link} to='/topics/read'>Show All</NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item as={Link} to='/topics/create'>Add Topic</NavDropdown.Item>
                                    </NavDropdown>
                                    <NavDropdown title="Posts" id="basic-nav-dropdown">
                                        <NavDropdown.Item as={Link} to='/posts/read'>Show All</NavDropdown.Item>
                                        <NavDropdown.Divider />
                                        <NavDropdown.Item as={Link} to='/posts/create'>Add Post</NavDropdown.Item>
                                    </NavDropdown>
                                </>
                            )}
                        </Nav>
                        <Nav>
                            {(ls.get('isLogin')) && (
                                <>
                                    <Nav.Link>{ls.get('name')}</Nav.Link>
                                    <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
                                </>
                            )}
                            {(!ls.get('isLogin')) && (
                                <>
                                    <Nav.Link as={Link} to='/login'>Login</Nav.Link>
                                    <Nav.Link as={Link} to='/register'>Register</Nav.Link>
                                </>
                            )}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <main>
                    <Switch>
                        <Route path='/' exact component={Home} />
                        <Route path='/topics/create' exact component={CreateTopics} />
                        <Route path='/topics/read' exact component={ReadTopics} />
                        <Route path='/topics/update/:id' component={UpdateTopics} />
                        <Route path='/topics/delete/:id' component={DeleteTopics} />
                        <Route path='/posts/create' exact component={CreatePosts} />
                        <Route path='/posts/read' exact component={ReadPosts} />
                        <Route path='/posts/update/:id' component={UpdatePosts} />
                        <Route path='/posts/delete/:id' component={DeletePosts} />
                        <Route path='/posts/where/:id' component={WherePosts} />
                        <Route
                            path='/login'
                            exact
                            render={(props) => (
                                <Login {...props} handleSetIsLogin={() => this.setState({isLogin:true})} />
                            )}
                        />
                        <Route path='/register' exact component={Register} />
                        <Route
                            path='/logout'
                            exact
                            render={(props) => (
                                <Logout {...props} handleSetIsLogin={() => this.setState({isLogin:false})} />
                            )}
                        />
                        <Route component={NoMatch} />
                    </Switch>
                </main>
            </div>
        );
    }
}

export default App;
