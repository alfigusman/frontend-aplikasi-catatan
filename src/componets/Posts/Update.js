import React, { Component } from "react"
import PostsDataService from "../../services/posts.services"
import TopicsDataService from "../../services/topics.services"
import ls from "local-storage"
import {Container, Form, Button} from "react-bootstrap"
import CKEditor from 'ckeditor4-react';

class Update extends Component {
    constructor(props) {
        super(props)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangePermission = this.onChangePermission.bind(this)
        this.onChangeTopic = this.onChangeTopic.bind(this)
        this.getPost = this.getPost.bind(this)
        this.getTopics = this.getTopics.bind(this)
        this.updatePost = this.updatePost.bind(this)

        this.state = {
            status: "",
            message: "",
            currentPost: {
                id: null,
                title: "",
                post: "",
                permission: "",
                topic: "",
                user: ""
            },
            topics: [],
        }
    }

    componentDidMount() {
        this.getPost(this.props.match.params.id)
        this.getTopics()
    }

    onChangeTitle(e) {
        const title = e.target.value
        this.setState(function(prevState) {
            return {
                currentPost: {
                    ...prevState.currentPost,
                    title: title
                }
            }
        })
    }

    onChangePermission(e) {
        const permission = e.target.value
        this.setState(prevState => ({
            currentPost: {
                ...prevState.currentPost,
                permission: permission
            }
        }))
    }

    onChangeTopic(e) {
        const topic = e.target.value
        this.setState(prevState => ({
            currentPost: {
                ...prevState.currentPost,
                topic: topic
            }
        }))
    }

    getTopics() {
        TopicsDataService.getAll()
            .then(res => {
                console.log(res)
                if (res.data.status){
                    this.setState({
                        status: res.data.status,
                        message: res.data.message
                    })
                    this.props.history.push('/posts/read')
                }else{
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        topics: res.data.data
                    })
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    getPost(id) {
        PostsDataService.get(id)
            .then(res => {
                console.log(res)
                if (res.data.status){
                    this.setState({
                        status: res.data.status,
                        message: res.data.message
                    })
                    this.props.history.push('/posts/read')
                }else{
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                    })
                    res.data.data.map(item => {
                        this.setState({
                            currentPost: {
                                id: item._id,
                                title: item.title,
                                post: item.post,
                                permission: item.permission,
                                topic: item.topic,
                                user: item.user
                            }
                        })
                    })
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({
                    status: e.data.status,
                    message: e.data.message,
                })
            })
    }

    updatePost() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.currentPost.title)
        bodyFormData.append('post', this.state.currentPost.post)
        bodyFormData.append('permission', this.state.currentPost.permission)
        bodyFormData.append('topic', this.state.currentPost.topic)
        bodyFormData.append('user', ls.get('id'))

        PostsDataService.update(this.state.currentPost.id, bodyFormData)
            .then(res => {
                console.log(res.data)
                this.setState({
                    status: res.data.status,
                    message: res.data.message
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    render() {
        const {status, message, currentPost, topics} = this.state
        return(
            <Container>
                {currentPost.id != null ? (
                    <Form>
                        <Form.Group>
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" value={currentPost.title} onChange={this.onChangeTitle} placeholder="Title" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Topics</Form.Label>
                            <Form.Control as="select" onChange={this.onChangeTopic}>
                                {topics.map((item, index) =>
                                    <option key={index} value={item._id} selected={item._id === currentPost.topic}>{item.title}</option>
                                )}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Permission</Form.Label>
                            <Form.Control as="select" onChange={this.onChangePermission}>
                                <option value="public" selected={currentPost.permission === 'public'}>Public</option>
                                <option value="private" selected={currentPost.permission === 'private'}>Private</option>
                            </Form.Control>
                        </Form.Group>
                            <Form.Group>
                                <Form.Label>Post</Form.Label>
                                <CKEditor
                                    data={currentPost.post}
                                    type="classic"
                                    onReady={ editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        this.setState(prevState => ({
                                            currentPost: {
                                                ...prevState.currentPost,
                                                post: data
                                            }
                                        }))
                                        /* console.log( { event, editor, data } ); */
                                    } }
                                    onBlur={ ( event, editor ) => {
                                        /* console.log( 'Blur.', editor ); */
                                    } }
                                    onFocus={ ( event, editor ) => {
                                        /* console.log( 'Focus.', editor ); */
                                    } }
                                />
                            </Form.Group>
                        <Button type='submit' className='btn btn-primary' onClick={this.updatePost}>Update</Button>
                    </Form>
                ) : (
                    <div>
                        {status ? <p>{message}</p> : null}
                    </div>
                )}
            </Container>
        )
    }

}

export default Update