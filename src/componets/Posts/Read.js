import React, { Component } from "react"
import PostsDataService from "../../services/posts.services"
import { Link } from "react-router-dom"
import ls from "local-storage"
import {Container, Row, Col, Button, Table} from "react-bootstrap"

class Read extends Component {
    constructor(props) {
        super(props)
        this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this)
        this.retrievePosts = this.retrievePosts.bind(this)
        this.searchTitle = this.searchTitle.bind(this)

        this.state = {
            status: "",
            message: "",
            posts: [],
            searchTitle: ""
        }
    }

    componentDidMount() {
        this.retrievePosts()
    }

    onChangeSearchTitle(e) {
        const searchTitle = e.target.value
        this.setState({
            searchTitle: searchTitle
        })
    }

    retrievePosts() {
        PostsDataService.findByUser(ls.get('id'))
            .then(res => {
                console.log(res.data)
                this.setState({
                    status: res.data.status,
                    message: res.data.message,
                    posts: res.data.data,
                })
            })
            .catch(e => {
                console.log(e)
                this.setState({
                    status: e.data.status,
                    message: e.data.message
                })
            })
    }

    searchTitle() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.searchTitle)
        if (this.state.searchTitle !== ""){
            PostsDataService.findByTitle(bodyFormData)
                .then(res => {
                    console.log(res.data)
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        posts: res.data.data
                    })
                })
                .catch(e => {
                    console.log(e)
                    this.setState({
                        status: e.data.status,
                        message: e.data.message
                    })
                })
        }else{
            PostsDataService.findByUser(ls.get('id'))
                .then(res => {
                    console.log(res.data)
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        posts: res.data.data
                    })

                })
                .catch(e => {
                    console.log(e)
                    this.setState({
                        status: e.data.status,
                        message: e.data.message
                    })
                })

        }
    }

    render() {
        const {searchTitle, posts} = this.state
        let num = 1
        return (
            <Container>
                <Row>
                    <Col sm={4}><h3>Tabel Posts</h3></Col>
                    <Col sm={8}>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Search by title" value={searchTitle} onChange={this.onChangeSearchTitle} />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.searchTitle}>
                                    Search
                                </button>
                            </div>
                        </div>
                    </Col>
                    <Col sm={12}>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Topic</th>
                                <th>Title</th>
                                <th>Permission</th>
                                <th>Date Create</th>
                                <th>Date Update</th>
                                <th colSpan='3'>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                posts != null && posts.map(post =>
                                    <>
                                        <tr>
                                            <td>{num++}</td>
                                            <td>{post.topics.map(item => item.title)}</td>
                                            <td>{post.title}</td>
                                            <td>{post.permission}</td>
                                            <td>{post.created_at}</td>
                                            <td>{post.updated_at}</td>
                                            <td className="text-center"><Button as={Link} to={"/posts/where/" + post._id} variant="success">Detail</Button></td>
                                            <td className="text-center"><Button as={Link} to={"/posts/update/" + post._id} variant="warning">Edit</Button></td>
                                            <td className="text-center"><Button as={Link} to={"/posts/delete/" + post._id} variant="danger">Delete</Button></td>
                                        </tr>
                                    </>
                                )
                            }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Read