import React, { Component } from "react"
import PostsDataService from "../../services/posts.services"
import {Container, Row, Col} from "react-bootstrap";
import ReactHtmlParser from 'react-html-parser';

class Where extends Component {
    constructor(props) {
        super(props)

        this.state = {
            status: "",
            message: "",
            posts:[]
        }
    }

    componentDidMount() {
        PostsDataService.get(this.props.match.params.id)
            .then(res => {
                console.log(res.data)
                if (res.data.status){
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                    })
                    this.props.history.push('/posts/read')
                }else{
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        posts: res.data.data
                    })
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({
                    status: e.data.status,
                    message: e.data.message
                })
            })
    }

    render() {
        const {posts} = this.state
        return(
            <Container>
                {posts != null && posts.map(post =>
                    <Row>
                        <Col sm={6}><h3>{post.title}</h3></Col>
                        <Col sm={6} className='text-right'>Permission : {post.permission}</Col>
                        <Col sm={12}>{ReactHtmlParser(post.post)}</Col>
                    </Row>
                )}
            </Container>
        )
    }

}

export default Where