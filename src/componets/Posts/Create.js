import React, {Component} from "react"
import PostsDataService from "../../services/posts.services"
import TopicsDataService from "../../services/topics.services";
import {Container, Form, Button} from "react-bootstrap"
import ls from "local-storage"

import CKEditor from 'ckeditor4-react';


class Create extends Component{
    constructor(props) {
        super(props)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangePermission = this.onChangePermission.bind(this)
        this.onChangeTopic = this.onChangeTopic.bind(this)
        this.getTopics = this.getTopics.bind(this)
        this.savePosts = this.savePosts.bind(this)
        this.newPosts = this.newPosts.bind(this)

        this.state = {
            status: "",
            message: "",
            title: "",
            post: "",
            permission: "",
            topic: "",
            user: ls.get('id'),
            topics: [],
            submitted: false
        }
    }

    componentDidMount() {
        this.getTopics()
    }

    onChangeTitle(e) {
        this.setState(prevState => ({
            title: e.target.value
        }))
    }

    onChangePermission(e) {
        this.setState(prevState => ({
            permission: e.target.value
        }))
    }

    onChangeTopic(e) {
        this.setState(prevState => ({
            topic: e.target.value
        }))
    }

    getTopics() {
        TopicsDataService.getAll()
            .then(res => {
                console.log(res)
                if (res.data.status){
                    this.setState({
                        status: res.data.status,
                        message: res.data.message
                    })
                    this.props.history.push('/posts/read')
                }else{
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        topics: res.data.data
                    })
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    savePosts() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.title)
        bodyFormData.append('post', this.state.post)
        bodyFormData.append('permission', this.state.permission)
        bodyFormData.append('topic', this.state.topic)
        bodyFormData.append('user', this.state.user)
        PostsDataService.create(bodyFormData)
            .then(res => {
                console.log(res.data)
                if (res.data.status){
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        title: res.data.title,
                        post: res.data.post,
                        permission: res.data.permission,
                        topic: res.data.topic,
                        user: res.data.user,
                        submitted: false
                    })
                }else{
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        title: res.data.title,
                        post: res.data.post,
                        permission: res.data.permission,
                        topic: res.data.topic,
                        user: res.data.user,
                        submitted: true
                    })
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    newPosts() {
        this.setState({
            status: "",
            message: "",
            title: "",
            post: "",
            permission: "",
            topic: "",
            submitted: false
        })
    }

    render() {
        return(
            <div>
                <Container>
                    {this.state.status ? <p>{this.state.message}</p> : null}
                    {this.state.submitted ? (
                        <div>
                            <h4>You submitted successfully!</h4>
                            <button className="btn btn-success" onClick={this.newPosts}>
                                Add
                            </button>
                        </div>
                    ) : (
                        <Form onSubmit={this.savePosts}>
                            <Form.Group>
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" value={this.state.title} onChange={this.onChangeTitle} placeholder="Title" required />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Topics</Form.Label>
                                <Form.Control as="select" onChange={this.onChangeTopic} required>
                                    <option>&laquo; Pilih &raquo;</option>
                                    {this.state.topics.map(item =>
                                        <option value={item._id}>{item.title}</option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Permission</Form.Label>
                                <Form.Control as="select" onChange={this.onChangePermission}>
                                    <option>&laquo; Pilih &raquo;</option>
                                    <option value="public">Public</option>
                                    <option value="private">Private</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Post</Form.Label>
                                    <CKEditor
                                        data={this.state.post}
                                        type="classic"
                                        onReady={ editor => {
                                            // You can store the "editor" and use when it is needed.
                                            console.log( 'Editor is ready to use!', editor );
                                        } }
                                        onChange={ ( event, editor ) => {
                                            const data = editor.getData();
                                            this.setState(prevState => ({
                                                post: data
                                            }))
                                            /* console.log( { event, editor, data } ); */
                                        } }
                                        onBlur={ ( event, editor ) => {
                                            /* console.log( 'Blur.', editor ); */
                                        } }
                                        onFocus={ ( event, editor ) => {
                                            /* console.log( 'Focus.', editor ); */
                                        } }
                                    />
                            </Form.Group>
                            <Button type='submit' className='btn btn-primary'>Save</Button>
                        </Form>
                    )}
                </Container>
            </div>
        )

    }
}

export default Create