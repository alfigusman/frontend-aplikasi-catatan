import React, {Component} from "react"
import UsersDataService from "../../services/users.services"
import ls from 'local-storage'

import logo from "../../images/logo.svg"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Card from "react-bootstrap/Card"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Alert from "react-bootstrap/Alert"

class Login extends Component{
    constructor(props) {
        super(props)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

        this.state = {
            status: false,
            message: '',
            errors:false,
            errorMessage: [],
            email: '',
            password: '',
            data: ''
        }
    }


    handleChangeEmail = event => {
        this.setState({ email: event.target.value })
    }

    handleChangePassword = event => {
        this.setState({ password: event.target.value })
    }

    handleSubmit = event => {
        event.preventDefault()

        const bodyFormData = new FormData()
        bodyFormData.append('email', this.state.email)
        bodyFormData.append('password', this.state.password)

        if(this.state.email === ""){
            this.setState({message:'Email tidak boleh kosong.', status: true})
            return false
        }
        if(this.state.password === ""){
            this.setState({message:'Password tidak boleh kosong', status: true})
            return false
        }

        UsersDataService.login(bodyFormData)
            .then(res => {
                if (res.data.status){
                    console.log(res)
                    if(typeof res.data.message === 'object'){
                        this.setState({
                            errors: res.data.status,
                            errorMessage: res.data.message,
                        })
                    }else{
                        this.setState({
                            status: res.data.status,
                            message: res.data.message,
                        })
                    }
                }else{
                    this.setState({data: res.data.data})
                    const self = this
                    Object.keys(this.state.data).map((key) => {
                        ls.set(key, self.state.data[key])
                    })
                    ls.set('isLogin', true)
                    ls.set('token', res.data.token)
                    this.props.history.push('/')
                    this.props.handleSetIsLogin()
                }
            })
            .catch(e => {
                console.log(e)
            })
    }


    render() {
        const notif = this.state.errorMessage
        return(
            <Container>
                <Row>
                    <Col md={true}>&nbsp;</Col>
                    <Col md={true}>
                        <Card>
                            <Row className="justify-content-md-center">
                                <Card.Img variant="top" src={logo} className="center-block" style={{width: '15rem'}} />
                            </Row>
                            <Card.Body>
                                <Card.Title className='text-center'>Login</Card.Title>
                                {
                                    this.state.errors ?
                                        Array.isArray(notif) ?
                                            notif.map(item => {return <Alert key={item} variant='danger'>{item}</Alert>}) :
                                            Object.keys(notif).map(function(key) {return <Alert key={key} variant='danger'>{key} : {notif[key]}</Alert>})
                                        : null
                                }
                                {
                                    this.state.status ? <Alert variant='danger'>{this.state.message}</Alert> : null
                                }
                                <Form onSubmit={this.handleSubmit}>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email" name="email" value={this.state.email} onChange={this.handleChangeEmail} placeholder="Email" required />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" name="password" value={this.state.password} onChange={this.handleChangePassword} placeholder="Password" required />
                                    </Form.Group>
                                    <Button variant="primary" type="submit" className="float-right">
                                        Sign-in
                                    </Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={true}>&nbsp;</Col>
                </Row>
            </Container>
        )
    }

}

export default Login
