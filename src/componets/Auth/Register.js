import React, {Component} from "react"
import UsersDataService from "../../services/users.services"
import logo from "../../images/logo.svg"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Card from "react-bootstrap/Card"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Container from "react-bootstrap/Container"
import Alert from "react-bootstrap/Alert"

class Register extends Component {
    constructor(props) {
        super(props)
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangePassword = this.handleChangePassword.bind(this)
        this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(this)

        this.state={
            status: false,
            message: '',
            errors: false,
            errorMessage: [],
            name: '',
            email: '',
            password: '',
            confirm_password: '',
        }
    }

    handleChangeName = e => {
        this.setState({name: e.target.value})
    }

    handleChangeEmail = e => {
        this.setState({email: e.target.value})
    }

    handleChangePassword = e => {
        this.setState({password: e.target.value})
    }

    handleChangeConfirmPassword = e => {
        this.setState({confirm_password: e.target.value})
    }

    handleSubmit = e => {
        e.preventDefault()

        const bodyFormData=new FormData()
        bodyFormData.append('name', this.state.name)
        bodyFormData.append('email', this.state.email)
        bodyFormData.append('password', this.state.password)
        bodyFormData.append('confirm_password', this.state.confirm_password)

        if (this.state.name === "") {
            this.setState({status: true, message: 'Name tidak boleh kosong.'})
            return false
        }
        if (this.state.email === "") {
            this.setState({status: true, message: 'Email tidak boleh kosong.'})
            return false
        }
        if (this.state.password === "") {
            this.setState({status: true, message: 'Password tidak boleh kosong'})
            return false
        }
        if (this.state.confirm_password === "") {
            this.setState({status: true, message: 'Confirm Password tidak boleh kosong.'})
            return false
        }
        if (this.state.password !== this.state.confirm_password) {
            this.setState({status: true, message: 'Password dan Confirm Password tidak sama.'})
            return false
        }

        UsersDataService.create(bodyFormData)
            .then(res => {
                if (res.data.status) {
                    console.log(res)
                    if (typeof res.data.message === 'object') {
                        this.setState({
                            errors: res.data.status,
                            errorMessage: res.data.message,
                        })
                    } else {
                        this.setState({
                            status: res.data.status,
                            message: res.data.message,
                        })
                    }
                } else {
                    this.props.history.push('/login')
                }
            })
            .catch(e => {
                console.log(e)
                this.setState({
                    status: e.data.status,
                    message: e.data.message
                })
            })
    }

    render() {
        const notif=this.state.errorMessage
        return (
            <Container>
                <Row>
                    <Col md={true}>&nbsp;</Col>
                    <Col md={true}>
                        <Card>
                            <Row className="justify-content-md-center">
                                <Card.Img variant="top" src={logo} style={{width: '15rem'}}
                                />
                            </Row>
                            <Card.Body>
                                <Card.Title className='text-center'>Register</Card.Title>
                                {this.state.errors ?
                                    Array.isArray(notif) ?
                                        notif.map(item => { return <Alert variant='danger'> {item}</Alert>})
                                        :
                                        Object.keys(notif).map(function(key) {
                                            return <Alert key={key} variant='danger'> {key}: {notif[key]}</Alert>})
                                    : null
                                } {
                                this.state.status ? < Alert variant='danger'> {this.state.message}</Alert> : null
                            }
                                <Form onSubmit={this.handleSubmit}>
                                    <Form.Group controlId="formBasicName">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text" name='name' value={this.state.name} onChange={this.handleChangeName} placeholder="Name" required />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email" name='email' value={this.state.email} onChange={this.handleChangeEmail} placeholder="Email" required />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" name='password' value={this.state.password} onChange={this.handleChangePassword} placeholder="Password" required />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicConfirmPassword">
                                        <Form.Label> Confirm Password</Form.Label>
                                        <Form.Control type="password" name='confirm_password' value={this.state.confirm_password} onChange={this.handleChangeConfirmPassword} placeholder="Confirm Password" required />
                                    </Form.Group>
                                    <Button variant="primary" type="submit" className="float-right">Sign - up</Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={true}>&nbsp;</Col>
                </Row>
            </Container>
        )
    }
}

export default Register