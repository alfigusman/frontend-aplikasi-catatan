import React, {Component} from "react"
import ls from 'local-storage'

class Logout extends Component{
    render() {
        ls.clear()
        this.props.history.push('/login')
        this.props.handleSetIsLogin()
        return(<div></div>)
    }

}

export default Logout
