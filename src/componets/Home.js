import React, {Component} from "react"
import {Row, Col, ListGroup, Card, Button} from "react-bootstrap"
import TopicsDataService from "../services/topics.services"
import PostsDataService from "../services/posts.services"
import {Link} from "react-router-dom";
import ls from "local-storage"

class Home extends Component{
    constructor(props) {
        super(props)
        this.state ={
            idTopic: 'public',
            topics: [],
            posts: []
        }
    }

    componentDidMount() {
        TopicsDataService.getAll()
            .then(res => {
                const topics = res.data.data
                this.setState({ topics })
            })
            .catch(res => {
                console.log(res)
            })
        if (this.state.idTopic === 'public'){
            PostsDataService.publicPosts()
                .then(res => {
                    const posts = res.data.data
                    this.setState({ posts })
                })
                .catch(res => {
                    console.log(res)
                })
        }
    }

    handleClick = (id) =>{
        if (id === 'public'){
            PostsDataService.publicPosts()
                .then(res => {
                    const posts = res.data.data
                    this.setState({ posts })
                })
                .catch(res => {
                    console.log(res)
                })
        }else if(id === 'private'){
            PostsDataService.findprivatePosts(ls.get('id'))
                .then(res => {
                    const posts = res.data.data
                    this.setState({ posts })
                })
                .catch(res => {
                    console.log(res)
                })
        } else{
            PostsDataService.findpublicPosts(id)
                .then(res => {
                    const posts = res.data.data
                    this.setState({ posts })
                })
                .catch(res => {
                    console.log(res)
                })
        }
    }

    render() {
        return(
            <Row style={{margin: '0 1rem'}}>
                <Col sm={2}>
                    <ListGroup defaultActiveKey='#public'>
                        <ListGroup.Item action href='#public' onClick={() => this.handleClick('public')}>Public</ListGroup.Item>
                        {ls.get('id') !== null ?
                            <ListGroup.Item action href='#private' onClick={() => this.handleClick('private')}>Private</ListGroup.Item>
                            :
                            null
                        }
                        {this.state.topics != null && this.state.topics.map(topic => <ListGroup.Item action onClick={() => this.handleClick(topic._id)} value={topic._id} href={'#' + topic._id}>{topic.title}</ListGroup.Item>)}
                    </ListGroup>
                </Col>
                <Col sm={10}>
                    <Row>
                        {this.state.posts != null && this.state.posts.map(post =>
                            <Col md={4} style={{marginBottom: '1rem'}}>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{post.title}</Card.Title>
                                        <Button variant="primary" className='btn-sm btn-block' as={Link} to={'/posts/where/' + post._id}>Detail</Button>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )}
                    </Row>
                </Col>
            </Row>
        )
    }

}

export default Home
