import React, { Component } from "react"
import TopicsDataService from "../../services/topics.services"
import ls from "local-storage"
import {Container, Form, Button} from "react-bootstrap"

class Update extends Component {
    constructor(props) {
        super(props)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangeTags = this.onChangeTags.bind(this)
        this.getTopic = this.getTopic.bind(this)
        this.updateTopic = this.updateTopic.bind(this)

        this.state = {
            status: "",
            message: "",
            currentTopic: {
                id: null,
                title: "",
                tags: "",
                user: ""
            }
        }
    }

    componentDidMount() {
        this.getTopic(this.props.match.params.id)
    }

    onChangeTitle(e) {
        const title = e.target.value

        this.setState(function(prevState) {
            return {
                currentTopic: {
                    ...prevState.currentTopic,
                    title: title
                }
            }
        })
    }

    onChangeTags(e) {
        const tags = e.target.value

        this.setState(prevState => ({
            currentTopic: {
                ...prevState.currentTopic,
                tags: tags
            }
        }))
    }

    getTopic(id) {
        TopicsDataService.get(id)
            .then(response => {
                response.data.data.map(item => {
                    this.setState({
                        currentTopic: {
                            id: item._id,
                            title: item.title,
                            tags: item.tags,
                            user: item.user
                        }
                    })
                })
                console.log(this.state.currentTopic)
            })
            .catch(e => {
                console.log(e)
            })
    }

    updateTopic() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.currentTopic.title)
        bodyFormData.append('tags', this.state.currentTopic.tags)
        bodyFormData.append('user', ls.get('id'))

        TopicsDataService.update(this.state.currentTopic.id, bodyFormData)
            .then(response => {
                console.log(response.data)
                this.setState({
                    message: response.data.message
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    render() {
        const {status, message, currentTopic} = this.state
        return(
            <Container>
                <h4>Topic</h4>
                {this.state.currentTopic ? (
                    <Form>
                        <Form.Group>
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" value={currentTopic.title} onChange={this.onChangeTitle} placeholder="Title" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Tags</Form.Label>
                            <Form.Control type="text" value={currentTopic.tags} onChange={this.onChangeTags} placeholder="Example : Tag1,Tag2,Tag3" />
                        </Form.Group>
                        <Button type='submit' className='btn btn-primary' onClick={this.updateTopic}>Update</Button>
                    </Form>
                ) : (
                    <div>
                        <br />
                        <p>Please click on a Topic...</p>
                    </div>
                )}
            </Container>
        )
    }

}

export default Update