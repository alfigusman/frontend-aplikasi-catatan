import React, { Component } from "react"
import TopicsDataService from "../../services/topics.services"
import { Link } from "react-router-dom"
import ls from "local-storage"
import {Container, Row, Col, Button, Table} from "react-bootstrap"

class Read extends Component {
    constructor(props) {
        super(props)
        this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this)
        this.retrieveTopics = this.retrieveTopics.bind(this)
        this.searchTitle = this.searchTitle.bind(this)

        this.state = {
            status: "",
            message: "",
            topics: [],
            searchTitle: ""
        }
    }

    componentDidMount() {
        this.retrieveTopics()
    }

    onChangeSearchTitle(e) {
        const searchTitle = e.target.value
        this.setState({
            searchTitle: searchTitle
        })
    }

    retrieveTopics() {
        TopicsDataService.findByUser(ls.get('id'))
            .then(res => {
                console.log(res.data)
                this.setState({
                    status: res.data.status,
                    message: res.data.message,
                    topics: res.data.data
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    searchTitle() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.searchTitle)
        if (this.state.searchTitle !== ""){
            TopicsDataService.findByTitle(bodyFormData)
                .then(res => {
                    console.log(res.data)
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        topics: res.data.data
                    })
                })
                .catch(e => {
                    console.log(e)
                })
        }else{
            TopicsDataService.findByUser(ls.get('id'))
                .then(res => {
                    console.log(res.data)
                    this.setState({
                        status: res.data.status,
                        message: res.data.message,
                        topics: res.data.data
                    })

                })
                .catch(e => {
                    console.log(e)
                })

        }
    }

    render() {
        const {searchTitle, topics} = this.state
        let num = 1
        return (
            <Container>
                <Row>
                    <Col sm={4}><h3>Tabel Topics</h3></Col>
                    <Col sm={8}>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Search by title" value={searchTitle} onChange={this.onChangeSearchTitle} />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.searchTitle}>
                                    Search
                                </button>
                            </div>
                        </div>
                    </Col>
                    <Col sm={12}>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Tags</th>
                                <th colSpan='2'>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                topics != null &&
                                topics.map(topic =>
                                    <>
                                        <tr>
                                            <td>{num++}</td>
                                            <td>{topic.title}</td>
                                            <td>{topic.tags.map(tag => tag + ', ')}</td>
                                            <td className="text-center"><Button as={Link} to={"/topics/update/" + topic._id} variant="warning">Edit</Button></td>
                                            <td className="text-center"><Button as={Link} to={"/topics/delete/" + topic._id} variant="danger">Delete</Button></td>
                                        </tr>
                                    </>
                                )
                            }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default Read