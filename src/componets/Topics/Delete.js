import React, { Component } from "react"
import TopicsDataService from "../../services/topics.services"

class Delete extends Component {
    constructor(props) {
        super(props)

        this.state = {
            status: "",
            message: "",
        }
    }

    componentDidMount() {
        TopicsDataService.delete(this.props.match.params.id)
            .then(res => {
                console.log(res.data)
                if (res.data.status) {
                    this.setState({
                        status: res.data.status,
                        message: res.data.message
                    })
                    this.props.history.push('/topics/read')
                } else {
                    this.setState({
                        status: res.data.status,
                        message: res.data.message
                    })
                    this.props.history.push('/topics/read')
                }
            })
            .catch(e => {
                this.setState({
                    status: e.data.status,
                    message: e.data.message
                })
                console.log(e)
            })
    }

    render() {
        return (<div></div>)
    }

}

    export default Delete