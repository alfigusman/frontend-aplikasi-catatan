import React, {Component} from "react"
import TopicsDataService from "../../services/topics.services"
import {Container, Form, Button} from "react-bootstrap"
import ls from "local-storage"

class Create extends Component{
    constructor(props) {
        super(props)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangeTags = this.onChangeTags.bind(this)
        this.saveTopics = this.saveTopics.bind(this)
        this.newTopics = this.newTopics.bind(this)

        this.state = {
            title: "",
            tags: "",
            user: ls.get('id'),
            submitted: false
        }
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        })
    }

    onChangeTags(e) {
        this.setState({
            tags: e.target.value
        })
    }

    saveTopics() {
        const bodyFormData = new FormData()
        bodyFormData.append('title', this.state.title)
        bodyFormData.append('tags', this.state.tags)
        bodyFormData.append('user', this.state.user)

        TopicsDataService.create(bodyFormData)
            .then(response => {
                this.setState({
                    title: response.data.title,
                    tags: response.data.tags,
                    user: response.data.user,
                    submitted: true
                })
                console.log(response.data)
            })
            .catch(e => {
                console.log(e)
            })
    }

    newTopics() {
        this.setState({
            title: "",
            tags: "",
            user: ls.get('id'),
            submitted: false
        })
    }

    render() {
        return(
            <div>
                <Container>
                        {this.state.submitted ? (
                            <div>
                                <h4>You submitted successfully!</h4>
                                <button className="btn btn-success" onClick={this.newTopics}>
                                    Add
                                </button>
                            </div>
                        ) : (
                            <Form>
                                <Form.Group controlId="formGroupTitle">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" value={this.state.title} onChange={this.onChangeTitle} placeholder="Title Topic" required />
                                </Form.Group>
                                <Form.Group controlId="formGroupTags">
                                    <Form.Label>Tags</Form.Label>
                                    <Form.Control type="text" value={this.state.tags} onChange={this.onChangeTags} placeholder="Example : Tag 1,Tag 2,Tag 3" required />
                                </Form.Group>
                                <Button onClick={this.saveTopics}>Submit</Button>
                            </Form>
                        )}
                </Container>
            </div>
        )
    }
}

export default Create