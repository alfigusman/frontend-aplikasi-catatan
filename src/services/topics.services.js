import http from "../http-common";

class TopicsDataService {
    getAll() {
        return http.get("Topics/");
    }

    get(id) {
        return http.get(`Topics/${id}`);
    }

    create(data) {
        return http.post("/Topics", data);
    }

    update(id, data) {
        return http.put(`Topics/${id}`, data);
    }

    delete(id) {
        return http.delete(`Topics/${id}`);
    }

    findByUser(id) {
        return http.get(`Topics/${id}/user`);
    }

    findByTitle(title) {
        return http.post(`Topics/title/`, title);
    }
}

export default new TopicsDataService();