import http from "../http-common";

class PostsDataService {
    getAll() {
        return http.get("Posts/");
    }

    get(id) {
        return http.get(`Posts/${id}`);
    }

    create(data) {
        return http.post("/Posts", data);
    }

    update(id, data) {
        return http.put(`Posts/${id}`, data);
    }

    delete(id) {
        return http.delete(`Posts/${id}`);
    }

    publicPosts(){
        return http.get(`Posts/public/permission`)
    }

    privatePosts(){
        return http.get(`Posts/private/permission`)
    }

    findpublicPosts(id){
        return http.get(`/Posts/${id}/topic/public`)
    }

    findprivatePosts(user){
        return http.get(`/Posts/${user}/topic/private`)
    }

    findByUser(id) {
        return http.get(`Posts/${id}/user`);
    }

    findByTitle(title) {
        return http.post(`Posts/title`, title);
    }
}

export default new PostsDataService();