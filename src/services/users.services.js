import http from "../http-common";

class UsersDataService {
    getAll() {
        return http.get("Users/");
    }

    get(id) {
        return http.get(`Users/${id}`);
    }

    create(data) {
        return http.post("/Users", data);
    }

    update(id, data) {
        return http.put(`Users/${id}`, data);
    }

    delete(id) {
        return http.delete(`Users/${id}`);
    }

    login(data) {
        return http.post(`Login`, data);
    }

    changePassword(data) {
        return http.post(`ChangePassword/`, data);
    }
}

export default new UsersDataService();