import axios from "axios"
import ls from "local-storage"

console.log(ls.get('token'))

export default axios.create({
    baseURL: "http://localhost:8080/",
    headers: {
        "Content-Type": "multipart/form-data",
        "Authorization": ls.get('token')
    }
});